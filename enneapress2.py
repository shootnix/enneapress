import sys
from enneapress.manage import Manage


if __name__ == '__main__':
    manager = Manage()
    manager.execute_command(sys.argv)
