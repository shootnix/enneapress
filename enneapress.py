import sys
import os
import shutil
import yaml
import sqlite3
import SimpleHTTPServer
import BaseHTTPServer
from enneapress.stream import Stream
from enneapress.util import read_file

sys.path.append('enneapress')


def print_usage():
    print('Usage: ')
    print("\tenneapress.py new [myBlog]")
    print("\tenneapress.py pub")
    print("\tenneapress.py server")

    exit(0)


def create_new(project_name):
    cwd = os.getcwd()
    mydir = os.path.dirname(os.path.abspath(__file__))

    print('mydir: ' + mydir)

    project_dir = os.path.join(cwd, project_name)
    force_rw = False
    if os.path.exists(project_dir) and os.path.isdir(project_dir):
        answer = raw_input('Dir ' + project_dir + ' is exists. Rewrite? [n] ')
        if answer == 'y':
            try:
                shutil.rmtree(project_dir)
            except:
                print('Can\'t remove directory, exit')
                exit(0)
        else:
            print('OK, bye ;-)')
            sys.exit()

    try:
        os.mkdir(project_dir)
    except:
        print('Can\'t create directory: ' + project_dir)
        exit(0)

    conf_dir = os.path.join(project_dir, 'conf')
    data_dir = os.path.join(project_dir, 'data')
    tmp_dir = os.path.join(project_dir, 'tmp')
    file_tranfer_dir = os.path.join(project_dir, 'files')

    for path in (conf_dir, data_dir, tmp_dir, file_tranfer_dir):
        try:
            os.mkdir(path)
        except:
            print('Can\'t create directory ' + path)
            print(sys.exc_info()[0])
            exit(0)
        else:
            print(' Created path ' + path)

    db_file = os.path.join(data_dir, project_name + '.db')
    config = {
        'project_dir': project_dir,
        'project_name': project_name,
        'db': {
            'driver': 'sqlite3',
            'connect': db_file
        },
        'avail_transfers': [
            'file', 'dropbox'
        ],
        'avail_converters': [
            'md', 'html'
        ],
        'log': {
            'level': 'debug'
        },
        'local': {
            'pub': {
                'dir': os.path.join(project_dir, '..', project_name + '_Pub')
            }
        }
    }

    # Create a database file:
    try:
        conn = sqlite3.connect(db_file)
        sql_schema = read_file(os.path.join(mydir, 'share/schema.sql'))
        cursor = conn.cursor()
        cursor.executescript(sql_schema)
        conn.commit()
        conn.close()
    except:
        print(' [!] Unable to create a sqlite3 database file ' + db_file)
        print(str(sys.exc_info()[0]))
    else:
        print(' Created sqlite3 database ' + db_file)

    # Create a schema file:
    sql_schema_file_path = os.path.join(data_dir, 'schema.sql')
    try:
        sql_schema_file = open(sql_schema_file_path, 'w')
        sql_schema_file.write(sql_schema)
        sql_schema_file.close()
    except:
        print(' [!] Unable to create schema file ' + sql_schema_file_path)
        print(str(sys.exc_info()[0]))
    else:
        print(' Created sql schema file ' + sql_schema_file_path)

    # Create a global_settings config file:
    config_file_path = os.path.join(conf_dir, 'global_settings.yaml')
    yaml_cfg = yaml.dump(config, default_flow_style=False)
    try:
        config_file = open(config_file_path, 'w')
        config_file.write(yaml_cfg)
        config_file.close()
    except:
        print(' [!] Unable to create settings file ' + config_file_path)
        print(sys.exc_info()[0])
    else:
        print(' Created main config file: ' + config_file_path)

    transfer_config_file_path = os.path.join(conf_dir, 'transfer_settings.yaml')
    transfer_config = {
        'file': {
            'module': 'file',
            'class': 'File',
            'params': {
                'from_dir': file_tranfer_dir
            }
        }
    }
    transfer_yaml_cfg = yaml.dump(transfer_config, default_flow_style=False)
    # Cretae transfer config:
    try:
        transfer_config_file = open(transfer_config_file_path, 'w')
        transfer_config_file.write(transfer_yaml_cfg)
        transfer_config_file.close()
    except:
        print(' [!] Unable to create transfer config ' + transfer_config_file_path)
        print(str(sys.exc_info()[0]))
    else:
        print(' Created transfer config file: ' + transfer_config_file_path)

    converter_config_file_path = os.path.join(conf_dir, 'converter_settings.yaml')
    converter_config = {
        'html': {
            'module': 'html',
            'class': 'Html',
            'params': {
                'file_extension': '.html'
            }
        },
        'mkdown': {
            'module': 'mkdown',
            'class': 'Markdown',
            'params': {
                'file_extension': '.md'
            }
        }
    }
    converter_yaml_cfg = yaml.dump(converter_config, default_flow_style=False)
    # Create converter config file:
    try:
        converter_config_file = open(converter_config_file_path, 'w')
        converter_config_file.write(converter_yaml_cfg)
        converter_config_file.close()
    except:
        print(' [!] Unable to create converter config file ' + converter_config_file_path)
        print(str(sys.exc_info()[0]))
    else:
        print(' Created converter config file: ' + converter_config_file_path)

    publisher_config_file_path = os.path.join(conf_dir, 'publisher_settings.yaml')
    publisher_config = {
        'default': {
            'module': 'default',
            'class': 'Default',
            'params': {
                'from_root': os.path.join(project_dir, 'tmp'),
                'to_root': os.path.join(project_dir, '..', project_name + '_Pub'),
                'page_types': '.phtml'
            }
        }
    }
    publisher_yaml_cfg = yaml.dump(publisher_config, default_flow_style=False)
    # Create publisher config file:
    try:
        publisher_config_file = open(publisher_config_file_path, 'w')
        publisher_config_file.write(publisher_yaml_cfg)
        publisher_config_file.close()
    except:
        print(' [!] Unable to create publisher config file ' + publisher_config_file_path)
        print(str(sys.exc_info()[0]))
    else:
        print(' Created publisher config file: ' + publisher_config_file_path)

    # Create layouts:
    try:
        shutil.copytree(
            os.path.join(mydir, 'enneapress', 'layouts'),
            os.path.join(project_dir, 'layouts')
        )
    except:
        print('Unable to create layouts dir')
        print(sys.exc_info()[0])
    else:
        print(' Copied layouts')

if __name__ == "__main__":
    script_name = sys.argv.pop(0)
    if len(sys.argv) == 0:
        print_usage()

    command = sys.argv.pop(0)
    if command == 'new':
        project_name = None
        if len(sys.argv) == 0:
            project_name = raw_input('Name your blog: ')
        else:
            project_name = sys.argv.pop(0)

        create_new(project_name)
    elif command == 'pub':
        print('Serving...')
        # 0. Create STREAM
        stream = Stream()

        stream.dbh()

        # 1. Transport
        transfers_settings = stream.settings('transfer')
        for transfer_name in transfers_settings.keys():
            stream.init_transfer(transfers_settings[transfer_name])
            stream.transfer.find_new()
            while stream.transfer.has_new():
                stream.transfer.recv_new()

        # 2. Converter
        converters_settings = stream.settings('converter')
        for converter_name in converters_settings.keys():
            stream.init_converter(converters_settings[converter_name])
            stream.converter.find_new()
            while stream.converter.has_new():
                stream.converter.convert_new()

        # 3. Publisher
        publisher_settings = stream.settings('publisher')
        for publisher_name in publisher_settings.keys():
            stream.init_publisher(publisher_settings[publisher_name])
            stream.publisher.find_new()
            while stream.publisher.has_new():
                stream.publisher.publish_new()
    elif command == 'server':
        stream = Stream()
        settings = stream.settings('global')
        print(repr(settings))
        os.chdir(settings['local']['pub']['dir'])
        address = ('', 9000)
        handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        httpd = BaseHTTPServer.HTTPServer(address, handler)
        print('Starting server on http://localhost:9000')
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            httpd.shutdown()
            httpd.server_close()
            print("Server stopped.")
    else:
        print('Uncnown command: ' + command)
        print_usage()
        #exit(0)
