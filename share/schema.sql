drop table if exists blogfiles;
create table blogfiles (
    id integer not null primary key autoincrement,
    relpath text not null unique,
    md5sum varchar(32) not null,
    raw_content text null,
    parsed_content text null,
    title varchar(128),
    create_date timestamp not null default CURRENT_DATE,
    pub_date timestamp null default null,

    is_transferred integer not null default 0,
    is_parsed integer not null default 0
);
create index 'blogfiles_id' on blogfiles (id);
create index 'blogfiles_relpath' on blogfiles (relpath);
create index 'blogfiles_is_transferred' on blogfiles (is_transferred);
create index 'blogfiles_is_parsed' on blogfiles (is_parsed);
