import os
import hashlib
import codecs
import re
import sys


def read_dir(dir_path):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(dir_path):
        for filename in filenames:
            files.append(dirpath + '/' + filename)

    return files


def uriquote(uristr):
    uristr = uristr.replace(' ', '%20')

    return uristr


def mkdir_deep(basedir, dirname):
    if re.search('^' + os.path.sep, dirname):
        dirname = "".join(list(dirname.replace(basedir, ''))[1:])
    dirs = dirname.split(os.path.sep)

    dirpath = basedir
    while len(dirs) > 0:
        dirpath = os.path.join(dirpath, dirs.pop(0))
        if not exists_dir(dirpath):
            try:
                os.mkdir(dirpath)
            except:
                print('[ERROR]: Can\'t create %s' % dirpath)
                print('[ERROR]: %s' % str(sys.exc_info()[0]))
                break


def exists_dir(dirname):

    return os.path.exists(dirname) and os.path.isdir(dirname)


def get_md5hex(obj):
    string = read_file(obj) if type(obj) is file else obj
    hasher = hashlib.md5()
    hasher.update(string)

    return hasher.hexdigest()


def read_file(obj, blocksize=65536):
    fh = None
    if type(obj) is file:
        fh = obj
    else:
        try:
            fh = open(obj, 'rb')
        except IOError as e:
            print('[ERROR]: %s' % str(e))
            return None

    content = ''
    buf = fh.read(blocksize)
    while len(buf) > 0:
        content += buf
        buf = fh.read(blocksize)

    return content


def write_file(filename, content):
    try:
        fh = codecs.open(filename, 'w', encoding='utf-8')
        fh.write(content)
        fh.close()
    except IOError as e:
        print('[ERROR]: %s' % str(e))


def uniquefy(mylist):
    res = list()
    map(lambda x: not x in res and res.append(x), mylist)

    return res
