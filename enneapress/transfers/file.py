import os
import datetime
import hashlib
import shutil

from enneapress.stream import Transfer
from enneapress.util import (read_dir, get_md5hex, mkdir_deep, exists_dir)
from random import randint


class File(Transfer):
    def __init__(self, params):
        self.db = None
        self.new_files = []

        self.path_from = params['from_dir']
        self.path_to = os.path.join(os.getcwd(), 'tmp')
        if 'to_dir' in params.keys():
            self.path_to = params['to_dir']

        self.log('debug', 'path to: ' + self.path_to)

    def find_new(self):
        result = False

        all_files = read_dir(self.path_from)
        self.log('debug', 'all_files = ' + repr(all_files))

        for file_path in all_files:
            self.check_transferred(file_path) or self.new_files.append(file_path)

        return self.count_new() > 0

    def recv_new(self):
        filepath_from = self.new_files.pop()

        self.log('debug', 'path from: ' + filepath_from)

        filename = os.path.basename(filepath_from)
        dirname = os.path.dirname(filepath_from).replace(self.path_from, self.path_to)

        self.log('debug', 'filename: ' + filename)
        self.log('debug', 'dirname: ' + dirname)

        if not exists_dir(dirname):
            mkdir_deep(self.path_to, dirname)

        filepath_to = os.path.join(dirname, filename)
        try:
            shutil.copy(filepath_from, filepath_to)
        except:
            self.log('error', 'Unable to copy file ' + filepath_from)
        else:
            self.set_transferred(filepath_to)

        return True

    def count_new(self):
        cnt_new = len(self.new_files)

        self.log('debug', 'cnt = ' + str(cnt_new))

        return cnt_new
