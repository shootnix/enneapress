#from enneapress.conf.global_settings import *
from enneapress.transfers import *
from enneapress.log import Logger
from enneapress.util import (read_file, get_md5hex)
from enneapress.config import (get_config)
from sql import *
import inspect
import os
import yaml


class Stream(object):
    def __init__(self):
        # Meta-Attrs:
        self.transfer = None
        self.converter = None
        self.publisher = None
        self.db = None

    #def settings(self, settings_name):
    #    settings_filename = settings_name + '_settings.yaml'
    #    settings_yaml = read_file(os.path.join(os.getcwd(), 'conf', settings_filename))
    #    if not settings_yaml:
    #        return None
    #
    #    return yaml.load(settings_yaml)
    def settings(self, settings_name):
        return get_config(settings_name)

    def log(self, level, msg, log_level='debug'):
        stack = inspect.stack()
        the_class = stack[1][0].f_locals["self"].__class__
        the_method = stack[1][0].f_code.co_name
        caller = str(the_class) + ' ' + str(the_method) + '()'

        settings = self.settings('global')
        if settings and settings['log']['level']:
            log_level = settings['log']['level']

        Logger(caller, level, msg, log_level)

    def init_transfer(self, opts):
        self.transfer = Transfer(opts)
        self.log('debug', 'Transfer initialized')

        return True if self.transfer else False

    def init_converter(self, opts):
        self.converter = Converter(opts)
        self.log('debug', 'Converter initialized')

        return True if self.converter else False

    def init_publisher(self, opts):
        self.publisher = Publisher(opts)
        self.log('debug', 'Publisher initialized')

        return True if self.publisher else False

    def dbh(self):
        global_settings = self.settings('global')
        if not global_settings:
            return None

        dbmodule = __import__(global_settings['db']['driver'])
        if self.db is None:
            self.db = dbmodule.connect(global_settings['db']['connect'])
            self.log('debug', 'DB Connected')
        else:
            try:
                cursor = self.db.cursor()
                cursor.execute('select 1')
                data = cursor.fetchone()
            except:
                self.db = dbmodule.connect(global_settings['db']['connect'])
                self.log('debug', 'DB Reconnected')

        return self.db

    def import_subclass(self):
        import_str_items = (
            'from enneapress',
            self.module_root,
            self.module_name
        )
        import_str = '.'.join(import_str_items) + ' import *'
        constructor_str = self.class_name + '(self.params)'

        exec(import_str)
        instance = eval(constructor_str)

        return instance


class Transfer(Stream):
    def __init__(self, opts):
        self.module_root = 'transfers'
        self.module_name = opts['module']
        self.class_name = opts['class']
        self.params = opts['params']

        self.t = self.import_subclass()
        self.path_to = self.t.path_to
        self.path_from = self.t.path_from

        self.db = None

    def find_new(self):
        has_new = self.t.find_new()
        self.log('debug', 'found: ' + repr(has_new))

        return has_new

    def has_new(self):
        cnt = self.t.count_new()

        return True if cnt > 0 else False

    def recv_new(self):
        return self.t.recv_new()

    def check_transferred(self, file_path):
        fh = open(file_path, 'rb')
        md5sum = get_md5hex(fh)
        relpath = file_path.replace(self.path_from + os.path.sep, '')
        Flavor.set(Flavor(paramstyle='qmark'))
        blogfiles = Table('blogfiles')
        select = blogfiles.select(blogfiles.id)
        select.where = (blogfiles.relpath == relpath) & (blogfiles.md5sum == md5sum) & (blogfiles.is_transferred == 1)

        self.log('debug', 'SQL = ' + str(select))

        dbh = self.dbh()
        dbh.text_factory = str
        cursor = dbh.cursor()
        sql, bind = tuple(select)
        cursor.execute(sql, bind)
        data = cursor.fetchall()

        self.log('debug', 'data = ' + repr(data))

        res = True if len(data) > 0 else False

        return res

    def set_transferred(self, file_path):
        filename = os.path.basename(file_path)
        title, ext = os.path.splitext(filename)
        relpath = file_path.replace(self.path_to + os.path.sep, '')

        self.log('debug', 'relpath=' + str(relpath))
        file_md5sum = get_md5hex(open(file_path, 'rb'))
        ### insert into database
        Flavor.set(Flavor(paramstyle='qmark'))
        blogfiles = Table('blogfiles')
        insert = blogfiles.insert(
            columns=[blogfiles.relpath, blogfiles.md5sum, blogfiles.title, blogfiles.is_transferred],
            values=[[unicode(relpath, 'utf-8'), file_md5sum, unicode(title, 'utf-8'), 1]]
        )
        #sql_stmt = """INSERT INTO blogfiles (relpath, md5sum, title, is_transferred)
        #              VALUES (?, ?, ?, ?)"""

        dbh = self.dbh()
        dbh.text_factory = str
        cursor = dbh.cursor()
        sql, bind = tuple(insert)

        self.log('debug', 'sql=' + sql)

        cursor.execute(sql, bind)
        dbh.commit()


class Converter(Stream):
    def __init__(self, opts):
        self.module_root = 'converters'
        self.module_name = opts['module']
        self.class_name = opts['class']
        self.params = opts['params']

        self.db = None

        self.c = self.import_subclass()

    def find_new(self):
        has_new = self.c.find_new()
        self.log('debug', 'found: ' + repr(has_new))

        return has_new

    def has_new(self):
        cnt = self.c.count_new()

        return True if cnt > 0 else False

    def check_converted(self, filepath):
        relpath = filepath.replace(self.work_dir + os.path.sep, '')

        #sql_stmt = """SELECT 1 FROM blogfiles
        #              WHERE relpath = ? AND is_parsed = ?"""
        Flavor.set(Flavor(paramstyle='qmark'))
        blogfiles = Table('blogfiles')
        select = blogfiles.select(blogfiles.id)
        select.where = (blogfiles.relpath == unicode(relpath, 'utf-8')) & (blogfiles.is_parsed == 1)

        dbh = self.dbh()
        dbh.text_factory = str
        cursor = dbh.cursor()
        sql, bind = tuple(select)

        self.log('debug', 'sql=' + sql)

        cursor.execute(sql, bind)
        data = cursor.fetchall()

        return True if len(data) > 0 else False

    def set_converted(self, relpath, raw, parsed):
        Flavor.set(Flavor(paramstyle='qmark'))
        blogfiles = Table('blogfiles')
        update = blogfiles.update(
            columns=[blogfiles.raw_content, blogfiles.parsed_content, blogfiles.is_parsed],
            values=[raw, parsed, 1],
            where=(blogfiles.relpath == unicode(relpath, 'utf-8'))
        )
        #sql_stmt = """UPDATE blogfiles
        #              SET
        #                  raw_content = ?,
        #                  parsed_content = ?,
        #                  is_parsed = 1
        #              WHERE
        #                  relpath = ?"""

        dbh = self.dbh()
        dbh.text_factory = str
        cursor = dbh.cursor()
        sql, bind = tuple(update)

        self.log('debug', 'sql=' + sql)
        self.log('debug', 'bind=' + repr(bind))

        cursor.execute(sql, bind)
        dbh.commit()

    def convert_new(self):
        return self.c.convert_new()


class Publisher(Stream):
    def __init__(self, opts):
        self.module_root = 'publishers'
        self.module_name = opts['module']
        self.class_name = opts['class']
        self.params = opts['params']

        self.pp = self.import_subclass()

    def get_not_published_id_list(self):
        new_items_id_list = list()
        Flavor.set(Flavor(paramstyle='qmark'))
        blogfiles = Table('blogfiles')
        select = blogfiles.select(blogfiles.id, order_by=blogfiles.create_date.desc)
        select.where = (blogfiles.pub_date == None)
        #sql_stmt = """SELECT id FROM blogfiles
        #              WHERE
        #                  pub_date IS NULL
        #              ORDER BY create_date DESC"""

        dbh = self.dbh()
        dbh.text_factory = str
        cursor = dbh.cursor()
        sql, bind = tuple(select)

        self.log('debug', 'sql=' + sql)

        cursor.execute(sql, bind)
        data = cursor.fetchall()

        for id in data:
            new_items_id_list.append(id[0])

        return new_items_id_list

    def find_new(self):
        return self.pp.find_new()

    def has_new(self):
        cnt = self.pp.count_new()

        return True if cnt > 0 else False

    def publish_new(self):
        return self.pp.publish_new()
