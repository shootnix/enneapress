import os

from enneapress.stream import Converter
from enneapress.util import (read_dir, read_file, write_file)


class Html(Converter):
    def __init__(self, params):
        self.db = None
        if 'work_dir' not in params.keys():
            self.work_dir = os.path.join(os.getcwd(), 'tmp')
        else:
            self.work_dir = params['work_dir']

        self.new_items = list()
        self.ext = params['file_extension']

    def find_new(self):
        all_files = read_dir(self.work_dir)

        self.log('debug', 'searching for new files')

        for file_path in all_files:
            name, ext = os.path.splitext(file_path)
            if ext != self.ext:
                continue

            if self.check_converted(file_path) is not True:
                self.new_items.append(file_path)

        self.log('debug', 'new_items found: ' + str(self.new_items))

        return True if len(self.new_items) > 0 else False

    def count_new(self):

        return len(self.new_items)

    def convert_new(self):
        myfile = self.new_items.pop()
        content = read_file(myfile)
        html = content

        relpath = myfile.replace(self.work_dir + os.path.sep, '')
        self.set_converted(relpath, content, html)

        try:
            os.remove(myfile)
        except:
            self.log('error', 'Unable to remove file ' + myfile)

        return True
