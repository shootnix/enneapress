import os
import markdown

from enneapress.stream import Converter
from enneapress.util import (read_dir, read_file, write_file)


class Markdown(Converter):
    def __init__(self, params):
        self.db = None
        work_dir = None
        if 'work_dir' not in params.keys():
            work_dir = os.path.join(os.getcwd(), 'tmp')
        else:
            work_dir = params['work_dir']

        self.work_dir = work_dir
        self.new_items = []
        self.ext = params['file_extension']

    def find_new(self):
        all_files = read_dir(self.work_dir)

        self.log('debug', 'all_files = ' + str(all_files))

        for file_path in all_files:
            name, ext = os.path.splitext(file_path)
            if ext != self.ext:
                continue

            if self.check_converted(file_path) is not True:
                self.new_items.append(file_path)

        self.log('debug', 'new_items found: ' + str(self.new_items))

        return True if len(all_files) > 0 else False

    def count_new(self):

        return len(self.new_items)

    def convert_new(self):
        myfile = self.new_items.pop()
        content = read_file(myfile)
        html = markdown.markdown(unicode(content, 'utf-8'))

        relpath = myfile.replace(self.work_dir + os.path.sep, '')
        self.set_converted(relpath, content, html)

        try:
            os.remove(myfile)
        except:
            self.log('error', 'Unable to remove file ' + myfile)

        return True
