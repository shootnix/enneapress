import os
import yaml

from enneapress.util import (read_file)


Config = {
    'global': None,
    'converter': None,
    'transfer': None,
    'publisher': None
}


def get_config(config_name):
    if not Config[config_name]:
        config_filename = config_name + '_settings.yaml'
        config_filepath = os.path.join(os.getcwd(), 'conf', config_filename)
        config_yaml = read_file(config_filepath)

        Config[config_name] = yaml.load(config_yaml)

    return Config[config_name]


def set_config(config_name, cfg):
    if config_name not in Config.keys():
        return False

    Config[config_name] = cfg

    return True
