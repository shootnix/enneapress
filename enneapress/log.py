import logging


class LoggerFilter(logging.Filter):

    def __init__(self, caller):
        self.caller = caller

    def filter(self, record):
        record.caller = self.caller

        return True


#logging.basicConfig(level=logging.ERROR, format='%(asctime)s [%(levelname)s] %(caller)s: %(message)s')


def Logger(caller, level, msg, log_level):
    myfilter = LoggerFilter(caller)
    logger = logging.getLogger('name')
    logger.addFilter(myfilter)

    logging_lvl = None
    if log_level == 'debug':
        logging_lvl = logging.DEBUG
    elif log_level == 'info':
        logging_lvl = logging.INFO
    elif log_level == 'warning':
        logging_lvl = logging.WARNING
    elif log_level == 'error':
        logging_lvl = logging.ERROR
    elif log_level == 'critical':
        logging_lvl = logging.CRITICAL

    logging.basicConfig(level=logging_lvl, format='%(asctime)s [%(levelname)s] %(caller)s: %(message)s')

    if level == 'debug':
        logger.debug(msg)
    elif level == 'info':
        logger.info(msg)
    elif level == 'warning':
        logger.warning(msg)
    elif level == 'error':
        logger.error(msg)
    elif level == 'critical':
        logger.critical(msg)
    else:
        pass
