import os
import sys
from enneapress.stream import Stream


class Manage(Stream):
    def __init__(self):
        self.module_root = 'manage_cmd'
        self.module_name = None
        self.class_name = 'Command'
        self.params = None

    def execute_command(self, argv):
        script_name = argv.pop(0)
        command_name = None

        if len(argv) == 0:
            command_name = 'help'
        else:
            command_name = argv.pop(0)

        self.params = argv
        self.module_name = command_name

        #try:
        command_instance = self.import_subclass()
        command_instance.handler()
        #except Exception as e:
        #    exc_type, exc_obj, exc_tb = sys.exc_info()
        #    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        #    print(exc_type, fname, exc_tb.tb_lineno)
        #    if e.message:
        #        print('[error]: %s' % e.message)
        #    print("Can't execute command, sorry")
