import os
import sys
import yaml
import shutil
from enneapress.stream import Publisher
from enneapress.util import *
from jinja2 import Template


class Default(Publisher):

    def __init__(self, params):
        self.db = None
        self.new_items = list()

        self.from_dir = params['from_root']
        self.document_root = params['to_root']
        self.page_types = params['page_types']
        self.layouts_dir = params['layouts_dir']
        self.layout_name = 'default'

    def find_new(self):
        self.new_items = self.get_not_published_id_list()

        self.log('debug', 'new_items=' + repr(self.new_items))

        return True if len(self.new_items) > 0 else False

    def count_new(self):
        cnt = len(self.new_items)

        return cnt

    def publish_new(self):
        item_id = self.new_items.pop()

        self.log('debug', 'item_id=' + repr(item_id))

        ### Old posts
        sql_stmt = """SELECT title, relpath
                      FROM blogfiles
                      WHERE
                          parsed_content IS NOT NULL
                              AND pub_date IS NOT NULL
                      ORDER BY pub_date"""

        db = self.dbh()
        cursor = db.cursor()
        old_posts = list()
        for row in cursor.execute(sql_stmt):
            (title, relpath) = row
            relpath, ext = os.path.splitext(relpath)
            relpath += '.html'
            post = {
                'title': title.decode('utf-8'),
                'uri': uriquote(relpath).decode('utf-8')
            }
            old_posts.append(post)

        self.log('debug', 'old_posts=' + repr(old_posts))

        ### New post
        sql_stmt = """SELECT relpath, title, parsed_content
                      FROM blogfiles
                      WHERE
                          id = ?"""

        cursor = db.cursor()
        try:
            cursor.execute(sql_stmt, (item_id,))
        except:
            self.log('critical', 'Unable to get new content from item #' + str(item_id))
            return False

        (relpath, title, parsed_content) = cursor.fetchone()

        settings = self.settings('global')
        project_name = settings['project_name']

        if parsed_content:
            post = {
                'title': title.decode('utf-8'),
                'content': parsed_content.decode('utf-8')
            }
            dirname, filename = os.path.split(relpath)
            filename, ext = os.path.splitext(filename)
            filename += '.html'
            self.log('debug', 'dirname=' + repr(dirname))
            filepath = os.path.join(self.document_root, dirname, filename)
            self.log('debug', relpath + ' goes to -> ' + filepath)

            index_template_path = os.path.join(self.layouts_dir, self.layout_name, 'index.html')
            index_template = read_file(index_template_path)
            template = Template(index_template)
            render = template.render(
                new_posts=[post],
                project_name=project_name,
                old_posts=old_posts
            )
            write_file(os.path.join(self.document_root, 'index.html'), render)

            mkdir_deep(self.document_root, dirname)
            page_template_path = os.path.join(self.layouts_dir, self.layout_name, 'post.html');
            page_template = read_file(page_template_path)
            template = Template(page_template)
            render = template.render(post=post)
            write_file(filepath, render)
        else:
            dirname, filename = os.path.split(relpath)
            if not exists_dir(os.path.join(self.document_root, dirname)):
                mkdir_deep(self.document_root, dirname)

            try:
                shutil.copy2(
                    os.path.join(self.from_dir, relpath),
                    os.path.join(self.document_root, relpath)
                )
            except IOError, e:
                self.log('error', 'unable to copy file to document_root: ' + str(e))
                return False

        sql_stmt = """UPDATE blogfiles SET pub_date=CURRENT_DATE
                      WHERE id=?"""

        cursor = db.cursor()
        try:
            cursor.execute(sql_stmt, (item_id,))
        except:
            self.log('error', 'Unable to set item as a published in DB')
        else:
            db.commit()

        return True
