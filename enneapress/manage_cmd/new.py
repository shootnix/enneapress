import os
import sys
import shutil
import sqlite3
import yaml
from enneapress.util import read_file


class Command():
    about = 'Creates a new blog project'

    def __init__(self, argv):
        self.argv = argv

    def handler(self):
        param = self.argv.pop(0) if len(self.argv) > 0 else 'help'

        if param == 'help':
            self.print_usage()
        else:
            self.create_new_blog(param)

    def print_usage(self):
        print("usage:")
        print("    new [blog name]")

    def create_new_blog(self, project_name):
        cwd = os.getcwd()
        mydir = os.path.dirname(os.path.abspath(__file__))  # !!!

        for i in range(0, 2):
            mydir, fn = os.path.split(mydir)

        project_dir = os.path.join(cwd, project_name)
        force_rw = False
        if os.path.exists(project_dir) and os.path.isdir(project_dir):
            answer = raw_input('Dir ' + project_dir + ' is exists. Rewrite? [n] ')
            if answer == 'y':
                try:
                    shutil.rmtree(project_dir)
                except Exception as e:
                    if e.message:
                        print('[error]: %s' % e.message)
                    print('Can\'t remove directory, exit')
                    exit(0)
            else:
                print('OK, bye ;-)')
                exit(0)

        try:
            os.mkdir(project_dir)
        except:
            print('Can\'t create directory: %s' % project_dir)
            exit(0)

        conf_dir = os.path.join(project_dir, 'conf')
        data_dir = os.path.join(project_dir, 'data')
        tmp_dir = os.path.join(project_dir, 'tmp')
        file_transfer_dir = os.path.join(project_dir, 'files')

        for path in (conf_dir, data_dir, tmp_dir, file_transfer_dir):
            try:
                os.mkdir(path)
            except:
                print('Can\'t create directory %s' % path)
                print(sys.exc_info()[0])
                exit(0)
            else:
                print(' Created path ' + path)

        db_file = os.path.join(data_dir, project_name + '.db')
        config = {
            'project_dir': project_dir,
            'project_name': project_name,
            'db': {
                'driver': 'sqlite3',
                'connect': db_file
            },
            'avail_transfers': [
                'file',
            ],
            'avail_converters': [
                'md',
                'html'
            ],
            'log': {
                'level': 'debug'
            },
            'local': {
                'pub': {
                    'dir': os.path.join(project_dir, '..', project_name + '_Pub')
                }
            }
        }

        # Create a database file:
        try:
            conn = sqlite3.connect(db_file)
            sql_schema = read_file(os.path.join(mydir, 'share/schema.sql'))
            cursor = conn.cursor()
            cursor.executescript(sql_schema)
            conn.commit()
            conn.close()
        except Exception as e:
            print(' [!] Unable to create a sqlite3 database file %s' % db_file)
            if e.message:
                print('[error]: %s' % e.message)
            print(str(sys.exc_info()[0]))
        else:
            print(' Created sqlite3 database %s' % db_file)

        # Create a schema file:
        sql_schema_file_path = os.path.join(data_dir, 'schema.sql')
        try:
            sql_schema_file = open(sql_schema_file_path, 'w')
            sql_schema_file.write(sql_schema)
            sql_schema_file.close()
        except:
            print(' [!] Unable to create schema file %s' % sql_schema_file_path)
            print(str(sys.exc_info()[0]))
        else:
            print(' Created sql schema file ' + sql_schema_file_path)

        # Create a global_settings config file:
        config_file_path = os.path.join(conf_dir, 'global_settings.yaml')
        yaml_cfg = yaml.dump(config, default_flow_style=False)
        try:
            config_file = open(config_file_path, 'w')
            config_file.write(yaml_cfg)
            config_file.close()
        except:
            print(' [!] Unable to create settings file %s' % config_file_path)
            print(sys.exc_info()[0])
        else:
            print(' Created main config file: %s' % config_file_path)

        transfer_config_file_path = os.path.join(conf_dir, 'transfer_settings.yaml')
        transfer_config = {
            'file': {
                'module': 'file',
                'class': 'File',
                'params': {
                    'from_dir': file_transfer_dir
                }
            }
        }
        transfer_yaml_cfg = yaml.dump(transfer_config, default_flow_style=False)
        # Cretae transfer config:
        try:
            transfer_config_file = open(transfer_config_file_path, 'w')
            transfer_config_file.write(transfer_yaml_cfg)
            transfer_config_file.close()
        except:
            print(' [!] Unable to create transfer config %s' % transfer_config_file_path)
            print(str(sys.exc_info()[0]))
        else:
            print(' Created transfer config file: %s' % transfer_config_file_path)

        converter_config_file_path = os.path.join(conf_dir, 'converter_settings.yaml')
        converter_config = {
            'html': {
                'module': 'html',
                'class': 'Html',
                'params': {
                    'file_extension': '.html'
                }
            },
            'mkdown': {
                'module': 'mkdown',
                'class': 'Markdown',
                'params': {
                    'file_extension': '.md'
                }
            }
        }
        converter_yaml_cfg = yaml.dump(converter_config, default_flow_style=False)
        # Create converter config file:
        try:
            converter_config_file = open(converter_config_file_path, 'w')
            converter_config_file.write(converter_yaml_cfg)
            converter_config_file.close()
        except:
            print(' [!] Unable to create converter config file %s' % converter_config_file_path)
            print(str(sys.exc_info()[0]))
        else:
            print(' Created converter config file: %s' % converter_config_file_path)

        publisher_config_file_path = os.path.join(conf_dir, 'publisher_settings.yaml')
        publisher_config = {
            'default': {
                'module': 'default',
                'class': 'Default',
                'params': {
                    'from_root': os.path.join(project_dir, 'tmp'),
                    'to_root': os.path.join(project_dir, '..', project_name + '_Pub'),
                    'page_types': '.phtml'
                }
            }
        }
        publisher_yaml_cfg = yaml.dump(publisher_config, default_flow_style=False)
        # Create publisher config file:
        try:
            publisher_config_file = open(publisher_config_file_path, 'w')
            publisher_config_file.write(publisher_yaml_cfg)
            publisher_config_file.close()
        except:
            print(' [!] Unable to create publisher config file %s' % publisher_config_file_path)
            print(str(sys.exc_info()[0]))
        else:
            print(' Created publisher config file: %s' % publisher_config_file_path)

        # Create layouts:
        try:
            shutil.copytree(
                os.path.join(mydir, 'enneapress', 'layouts'),
                os.path.join(project_dir, 'layouts')
            )
        except:
            print('Unable to create layouts dir')
            print(sys.exc_info()[0])
        else:
            print(' Copied layouts')
