from enneapress.util import (read_dir, uniquefy)
import os


class Command():
    about = 'Prints help info'

    def __init__(self, argv):
        self.argv = argv

    def handler(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        files = read_dir(dir_path)
        commands = list()
        for filepath in files:
            filename = os.path.basename(filepath)
            (root, ext) = os.path.splitext(filename)
            if root == '__init__':
                continue

            commands.append(root)

        commands = uniquefy(commands)
        print('[commands]:')
        for cmd in commands:
            print('    %s' % cmd)


