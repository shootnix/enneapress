from enneapress.stream import Stream


class Command():
    about = 'Public new posts'

    def __init__(self, argv):
        self.argv = argv

    def handler(self):
        self.publish()

    def publish(self):
        stream = Stream()
        stream.dbh()  # init database connection

        # 1. Transfer
        transfers_settings = stream.settings('transfer')
        for transfer_name in transfers_settings.keys():
            stream.init_transfer(transfers_settings[transfer_name])
            stream.transfer.find_new()
            while stream.transfer.has_new():
                stream.transfer.recv_new()

        # 2. Converter
        converters_settings = stream.settings('converter')
        for converter_name in converters_settings.keys():
            stream.init_converter(converters_settings[converter_name])
            stream.converter.find_new()
            while stream.converter.has_new():
                stream.converter.convert_new()

        # 3. Publisher
        publisher_settings = stream.settings('publisher')
        for publisher_name in publisher_settings.keys():
            stream.init_publisher(publisher_settings[publisher_name])
            stream.publisher.find_new()
            while stream.publisher.has_new():
                stream.publisher.publish_new()
