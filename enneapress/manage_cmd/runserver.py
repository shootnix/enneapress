import os
from enneapress.stream import Stream
import SimpleHTTPServer
import BaseHTTPServer


class Command():
    about = 'Runs simple static server'

    def __init__(self, argv):
        self.argv = argv

    def handler(self):
        param = self.argv.pop(0) if len(self.argv) else None

        if param is None or param != 'help':
            self.run_server(param)
        elif param == 'help':
            self.print_usage()

    def print_usage(self):
        print('usage:')
        print('    runserver <port>')

    def run_server(self, port):
        if port is None:
            port = 9000

        stream = Stream()
        settings = stream.settings('global')
        os.chdir(settings['local']['pub']['dir'])
        address = ('', int(port))
        handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        httpd = BaseHTTPServer.HTTPServer(address, handler)

        print('Starting server on http://localhost:%s' % port)
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            httpd.shutdown()
            httpd.server_close()
            print("Server stopped.")


