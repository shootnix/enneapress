import unittest
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from enneapress.stream import Stream
from enneapress.util import *
import enneapress.config


global_settings = {
    'project_dir': '/tmp/enneapress/Test',
    'project_name': 'Test',
    'db': {
        'driver': 'sqlite3',
        'connect': 'Test.db'
    },
    'avail_transfers': ['file', 'dropbox'],
    'avail_converters': ['md', 'html'],
    'log': {
        'level': 'debug'
    },
    'local': {
        'pub': {
            'dir': '/tmp/enneapress/test/../Test_pub'
        }
    }
}

transfer_settings = {
    'file': {
        'module': 'file',
        'class': 'File',
        'params': {
            'from_dir': '/tmp/enneapress/from'
        }
    }
}

converter_settings = {
    'html': {
        'module': 'html',
        'class': 'Html',
        'params': {
            'file_extension': '.html'
        }
    },
    'mkdown': {
        'module': 'mkdown',
        'class': 'Markdown',
        'params': {
            'file_extension': '.md'
        }
    }
}

publisher_settings = {
    'default': {
        'module': 'default',
        'class': 'Default',
        'params': {
            'from_root': '/tmp/enneapress/from',
            'to_root': '/tmp/enneapress/to',
            'page_types': '.phtml'
        }
    }
}


class CoreTest(unittest.TestCase):
    # Config
    def test_config_set_config_unknown(self):
        self.assertEqual(enneapress.config.set_config('unknown', dict()), False)

    def test_config_set_config_global(self):
        self.assertEqual(enneapress.config.set_config('global', {'foo': 'bar'}), True)
        self.assertEqual(enneapress.config.get_config('global'), {'foo': 'bar'})

    def test_config_get_config_global(self):
        enneapress.config.Config['global'] = {'foo': 'bar'}
        self.assertEqual(enneapress.config.get_config('global'), {'foo': 'bar'})

    def test_config_set_config_transfer(self):
        self.assertEqual(enneapress.config.set_config('transfer', {'foo': 'bar'}), True)
        self.assertEqual(enneapress.config.get_config('transfer'), {'foo': 'bar'})

    def test_config_get_config_transfer(self):
        enneapress.config.Config['transfer'] = {'foo': 'bar'}
        self.assertEqual(enneapress.config.get_config('transfer'), {'foo': 'bar'})

    def test_config_set_config_converter(self):
        self.assertEqual(enneapress.config.set_config('converter', {'foo': 'bar'}), True)
        self.assertEqual(enneapress.config.get_config('converter'), {'foo': 'bar'})

    def test_config_get_config_converter(self):
        enneapress.config.Config['converter'] = {'foo': 'bar'}
        self.assertEqual(enneapress.config.get_config('converter'), {'foo': 'bar'})

    def test_config_set_config_publisher(self):
        self.assertEqual(enneapress.config.set_config('publisher', {'foo': 'bar'}), True)
        self.assertEqual(enneapress.config.get_config('publisher'), {'foo': 'bar'})

    def test_config_get_config_publisher(self):
        enneapress.config.Config['publisher'] = {'foo': 'bar'}
        self.assertEqual(enneapress.config.get_config('publisher'), {'foo': 'bar'})

    # Stream
    def test_stream_init(self):
        self.failUnless(Stream())

    def test_stream_settings(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.settings('global'), global_settings)

    def test_stream_log_info(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.log('info', 'test'), None)

    def test_stream_log_debug(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.log('debug', 'test'), None)

    def test_stream_log_warning(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.log('warning', 'test'), None)

    def test_stream_log_error(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.log('error', 'test'), None)

    def test_stream_log_critical(self):
        enneapress.config.set_config('global', global_settings)
        stream = Stream()
        self.assertEqual(stream.log('critical', 'test'), None)

    def test_stream_init_transfer(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        stream = Stream()
        opts = stream.settings('transfer')
        transfer_name = opts.keys().pop()
        self.failUnless(stream.init_transfer(opts[transfer_name]))
        self.assertEqual(stream.transfer.class_name, opts[transfer_name]['class'])

    def test_stream_init_converter(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        stream = Stream()
        opts = stream.settings('converter')
        converter_name = opts.keys().pop()
        self.failUnless(stream.init_converter(opts[converter_name]))
        self.assertEqual(stream.converter.class_name, opts[converter_name]['class'])

    def test_stream_init_publisher(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('publisher', publisher_settings)
        stream = Stream()
        opts = stream.settings('publisher')
        publisher_name = opts.keys().pop()
        self.failUnless(stream.init_publisher(opts[publisher_name]))
        self.assertEqual(stream.publisher.class_name, opts[publisher_name]['class'])

    # Util
    #def test_util_read_dir(self):
    #    self.assertEqual(read_dir('unknown'), [])
    #    share_dir = ['share/enneapress.db', 'share/schema.sql']
    #    self.assertEqual(read_dir('share'), share_dir)

    def test_util_uriquote(self):
        self.assertEqual(uriquote('hello'), 'hello')
        self.assertEqual(uriquote('hello world'), 'hello%20world')

    #def test_util_mkdir_deep(self):
    #    pass
    #
    #def test_util_exists_dir(self):
    #    pass
    #
    #def test_util_get_md5hex(self):
    #    pass
    #
    #def test_util_read_file(self):
    #    pass
    #
    #def test_util_write_file(self):
    #    pass
    #
    #def test_util_uniquefy(self, ):
    #    pass


if __name__ == "__main__":
    unittest.main()
