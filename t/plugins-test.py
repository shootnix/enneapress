import unittest
import sys
import os
import sqlite3
import shutil

from sql import *

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.dirname(__file__))

from enneapress.transfers.file import File

from enneapress.converters.html import Html
from enneapress.converters.mkdown import Markdown

from enneapress.publishers.default import Default

from enneapress.stream import Stream
from enneapress.util import *
import enneapress.config


global_settings = {
    'project_dir': '/tmp/enneapress/Test',
    'project_name': 'Test',
    'db': {
        'driver': 'sqlite3',
        'connect': '/tmp/enneapress/Test.db'
    },
    'avail_transfers': ['file', 'dropbox'],
    'avail_converters': ['md', 'html'],
    'log': {
        'level': 'debug'
    },
    'local': {
        'pub': {
            'dir': '/tmp/enneapress/test/../Test_pub'
        }
    }
}

transfer_settings = {
    'file': {
        'module': 'file',
        'class': 'File',
        'params': {
            'from_dir': '/tmp/enneapress/from',
            'to_dir': '/tmp/enneapress/to'
        }
    }
}

converter_settings = {
    'html': {
        'module': 'html',
        'class': 'Html',
        'params': {
            'file_extension': '.html',
            'work_dir': '/tmp/enneapress/to'
        }
    },
    'mkdown': {
        'module': 'mkdown',
        'class': 'Markdown',
        'params': {
            'file_extension': '.md',
            'work_dir': '/tmp/enneapress/to'
        }
    }
}

publisher_settings = {
    'default': {
        'module': 'default',
        'class': 'Default',
        'params': {
            'from_root': '/tmp/enneapress/from',
            'to_root': '/tmp/enneapress/to',
            'page_types': '.phtml',
            'layouts_dir': '/tmp/enneapress'
        }
    }
}


def bootstrap():
    if os.path.exists('/tmp/enneapress') and os.path.isdir('/tmp/enneapress'):
        shutil.rmtree('/tmp/enneapress')

    os.mkdir('/tmp/enneapress')
    os.mkdir('/tmp/enneapress/Test')
    os.mkdir('/tmp/enneapress/from')
    os.mkdir('/tmp/enneapress/to')

    try:
        conn = sqlite3.connect(global_settings['db']['connect'])
        cursor = conn.cursor()
        sql_script = read_file('share/schema.sql')
        print('SQL: %s' % sql_script)
        cursor.executescript(sql_script)
        conn.commit()
        conn.close()
    except:
        print('[DB ERROR]')

    return True


def clean():
    shutil.rmtree('/tmp/enneapress')

    return True


class PluginsTest(unittest.TestCase):
    # transfer.file
    def test_transfer_file_init(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('transfer')

        self.failUnless(File(opts['file']['params']))

    def test_transfer_file_find_new(self):
        write_file('/tmp/enneapress/from/test.txt', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('transfer')
        s.init_transfer(opts['file'])

        self.assertEqual(s.transfer.find_new(), True)

        os.unlink('/tmp/enneapress/from/test.txt')

    def test_transfer_file_has_new(self):
        write_file('/tmp/enneapress/from/test.txt', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('transfer')
        s.init_transfer(opts['file'])
        s.transfer.find_new()

        self.assertEqual(s.transfer.has_new(), True)

        os.unlink('/tmp/enneapress/from/test.txt')

    def test_transfer_file_recv_new(self):
        write_file('/tmp/enneapress/from/test.txt', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('transfer')
        s.init_transfer(opts['file'])
        s.transfer.find_new()

        self.assertEqual(s.transfer.recv_new(), True)
        os.unlink('/tmp/enneapress/to/test.txt')

    # converter.html
    def test_converter_html_init(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')

        self.failUnless(Html(opts['html']['params']))

    def test_converter_html_find_new(self):
        write_file('/tmp/enneapress/to/test.html', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['html'])

        self.assertEqual(s.converter.find_new(), True)

        os.unlink('/tmp/enneapress/to/test.html')

    def test_converter_html_has_new(self):
        write_file('/tmp/enneapress/to/test.html', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['html'])

        s.converter.find_new()
        self.assertEqual(s.converter.has_new(), True)

        os.unlink('/tmp/enneapress/to/test.html')

    def test_converter_html_convert_new(self):
        write_file('/tmp/enneapress/to/test.html', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['html'])

        s.converter.find_new()
        self.assertEqual(s.converter.convert_new(), True)
        self.assertEqual(read_dir('/tmp/enneapress/to'), [])

    # converter.mkdown
    def test_converter_mkdown_init(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')

        self.failUnless(Html(opts['mkdown']['params']))

    def test_converter_mkdown_find_new(self):
        write_file('/tmp/enneapress/to/test.md', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['mkdown'])

        self.assertEqual(s.converter.find_new(), True)

        os.unlink('/tmp/enneapress/to/test.md')

    def test_converter_mkdown_has_new(self):
        write_file('/tmp/enneapress/to/test.md', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['mkdown'])

        s.converter.find_new()
        self.assertEqual(s.converter.has_new(), True)

        os.unlink('/tmp/enneapress/to/test.md')

    def test_converter_mkdown_convert_new(self):
        write_file('/tmp/enneapress/to/test.md', 'test')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('converter', converter_settings)
        s = Stream()
        opts = s.settings('converter')
        s.init_converter(opts['mkdown'])

        s.converter.find_new()
        self.assertEqual(s.converter.convert_new(), True)
        self.assertEqual(read_dir('/tmp/enneapress/to'), [])

    # publisher.default
    def test_publisher_default_init(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('publisher', publisher_settings)
        s = Stream()
        opts = s.settings('publisher')

        self.failUnless(Default(opts['default']['params']))

    def test_publisher_default_find_new(self):
        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('publisher', publisher_settings)
        enneapress.config.set_config('converter', converter_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('publisher')
        s.init_publisher(opts['default'])

        opts = s.settings('converter')
        s.init_converter(opts['html'])

        opts = s.settings('transfer')
        s.init_transfer(opts['file'])

        write_file('/tmp/enneapress/to/test.md', 'test')
        s.transfer.set_transferred('/tmp/enneapress/to/test.md')
        s.converter.set_converted('rel/path', 'test', 'test')

        self.assertEqual(s.publisher.find_new(), True)

        dbh = s.dbh()
        cursor = dbh.cursor()
        cursor.execute('delete from blogfiles')
        dbh.commit()

    def test_publisher_default_publish_new(self):
        os.mkdir('/tmp/enneapress/default')
        write_file('/tmp/enneapress/default/index.html', 'index')
        write_file('/tmp/enneapress/default/post.html', 'post')

        enneapress.config.set_config('global', global_settings)
        enneapress.config.set_config('publisher', publisher_settings)
        enneapress.config.set_config('converter', converter_settings)
        enneapress.config.set_config('transfer', transfer_settings)
        s = Stream()
        opts = s.settings('publisher')
        s.init_publisher(opts['default'])

        opts = s.settings('converter')
        s.init_converter(opts['html'])

        opts = s.settings('transfer')
        s.init_transfer(opts['file'])

        write_file('/tmp/enneapress/to/test.md', 'test')
        s.transfer.set_transferred('/tmp/enneapress/to/test.md')
        s.converter.set_converted('test.md', 'test', 'test')

        s.publisher.find_new()
        self.assertEqual(s.publisher.publish_new(), True)

        dbh = s.dbh()
        cursor = dbh.cursor()
        cursor.execute('delete from blogfiles')
        dbh.commit()

        shutil.rmtree('/tmp/enneapress/default')

if __name__ == '__main__':
    bootstrap()
    unittest.main()
